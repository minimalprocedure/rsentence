# Package

version       = "0.1.0"
author        = "Massimo Maria Ghisalberti"
description   = "markov chain test"
license       = "MIT"
srcDir        = "src"
bin           = @["main"]

# Dependencies

requires "nim >= 1.0.4"
