import
  tables,
  strutils,
  sequtils,
  random,
  os,
  utils

from times import getTime, toUnix, nanosecond

type
  ChainTable = OrderedTable[seq[string], seq[string]]
  #KeyValue = tuple[key: seq[string], value: seq[string]]
  SeqOfSeqStrings = seq[seq[string]]
  SplittedLine = tuple[begin: seq[string], words: seq[string]]
  SplittedLines = tuple[begins: SeqOfSeqStrings, words: seq[string]]

const
  Terminators = {'.', '!', '?'}
  Separators = {',', ';'}

proc findPunctuators(word: string): seq[string] {.noSideEffect.} =
  if word.last in Terminators or word.last in Separators:
    @[word[0..word.high - 1], $word.last]
  else:
    @[word]

proc analyzeWord(acc: seq[string], word: string): seq[string] {.noSideEffect.} =
  if not word.isEmpty: acc & findPunctuators(word)
  else: acc

proc mchain*(words: seq[string], order: int = 2): OrderedTable[seq[string], seq[
    string]] {.noSideEffect.} =
  let delta = order - 1
  for i in 0..words.high - order:
    let key: seq[string] = words[i..i+delta]
    let inc = i + order
    let word = words[inc] #if i < inc: words[inc] else: ""
    if(result.contains(key)): result[key].add(word)
    else: result[key] = @[word]
  result

proc initRandomizer() =
  let now = getTime()
  randomize(now.toUnix * 1000000000 + now.nanosecond)

proc truncSeqAtTerminator(sentence: string): string {.noSideEffect.} =
  var pos = -1
  if sentence.contains(Terminators):
    for t in items(Terminators):
      pos = sentence.find(t)
      if pos >= 0: break
  if pos >= 0: sentence[0..pos] else: sentence

proc joinSeqFragment(s: seq[string]): string {.noSideEffect.} =
  let op = proc(acc: string, frag: string): string =
    if frag[0] in Terminators or frag[0] in Separators:
      acc & frag
    else: acc & " " & frag
  truncSeqAtTerminator(fold(s, op, ""))

proc randomFragment(chain: ChainTable, key: seq[string]): seq[string] =
  let value = chain.getOrDefault(key)
  key & (if value.isEmpty: "." else: value[rand(value.len - 1)])

proc walkChain(chain: ChainTable, key: seq[string], order: int = 2): string =
  result & (if not key.last.contains(Terminators):
    let frag = randomFragment(chain, key)
    result.add(joinSeqFragment(frag.tail(order)))
    chain.walkChain(frag.tail(frag.len-order), order)
  else: "")

proc generateRandomText(chain: ChainTable, begins: SeqOfSeqStrings,
    order: int = 2): string =
  let begin = begins.rndElement
  result.add(capitalizeAscii(begin.join(" ")) & " ")
  result.add(chain.walkChain(begin, order).strip)

proc splitLine(line: string, order: int = 2): SplittedLine = #{.noSideEffect.} =
  let words = line.strip.split(Whitespace)
  result.begin = if words.len > order: words[0..order-1] else: words
  result.words = fold(words, analyzeWord, @[])

proc loadFile(f: string, order: int = 2): SplittedLines =
  for line in lines(f):
    if not line.isEmpty:
      let splitted = line.splitLine(order)
      result.begins.add(splitted.begin)
      result.words.add(splitted.words)

proc loadFiles(f: string, order: int = 2): SplittedLines =
  let fInfo = getFileInfo(f)
  if fInfo.kind == pcFile:
    result = loadFile(f, order)
  else:
    for f in walkFiles(normalizedPath(f & "/" & "*.*")):
      let data = loadFile(f, order)
      result.begins.add(data.begins)
      result.words.add(data.words)

proc printChain(chain: ChainTable) =
  for k in chain.keys:
    echo "KEY:[$#] -> VALUE:[$#]" % [k.join(" "), chain[k].join(" ")]

proc generateTextFromData*[T](f: T, order: int = 2, num: int = 1,
    debug: bool = false): string =
  initRandomizer()
  let beginAndWords = loadFiles(f, order)
  let chain = mchain(beginAndWords.words, order)
  if debug:
    echo "-------------- BEGIN CHAIN ----------------"
    printChain(chain)
    echo "-------------- END CHAIN ----------------\n"
  for n in 1..num:
    result.add(generateRandomText(chain, beginAndWords.begins, order) & " ")
