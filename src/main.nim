import rsentences, os, strutils, parseopt

proc run(): string =

  const UsageText = "usage: rsentence <-t|--template>:templatefile.txt [<-o|--order>:num [=1..5]] [<-n|--num>:num] [<-d|--debug>]"
  const ErrorFileNotFound = "Template file required."

  let cmdSeq = commandLineParams()

  if cmdSeq.len == 0:
    return UsageText

  var cmd = initOptParser(cmdSeq, shortNoVal = {'d'}, longNoVal = @["debug"])

  var order = 2
  var num = 1
  var tpl = ""
  var debug = false

  while true:
    cmd.next()
    case cmd.kind
    of cmdEnd:
      break
    of cmdShortOption, cmdLongOption:
      case cmd.key:
      of "o", "order":
        order = parseInt(cmd.val)
        if order > 5: order = 5
        if order < 1: order = 1
      of "n", "num":
        num = parseInt(cmd.val)
      of "d", "debug":
        echo "debug"
        debug = true
      of "t", "template":
        if fileExists(cmd.val) or dirExists(cmd.val):
          tpl = cmd.val
        else:
          result = ErrorFileNotFound
      else: result = UsageText
    of cmdArgument:
      result = UsageText

  if tpl.len > 0:
    result = generateTextFromData(tpl, order, num, debug)

when isMainModule:
  echo run()
