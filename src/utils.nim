type
  Reducer*[V, R] = proc(res: R, val: V): R {.closure.}

template isEmpty*[V](sequence: openArray[V]): bool = sequence.len == 0

template last*(s: string): char =
  assert(s.len > 0, "Empty string!")
  s[s.high]

template last*[T](s: openArray[T]): T =
  assert(s.len > 0, "Empty collection!")
  s[s.high]

template first*[T](s: openArray[T]): T =
  assert(s.len > 0, "Empty collection!")
  s[0]

template tail*[T](s: openArray[T], fromIndex: int = 1): auto =
  assert(s.len > 0, "Empty collection!")
  s[fromIndex..s.high]

template head*[T](s: openArray[T], endIndex: int = 0): auto =
  assert(s.len > 0, "Empty collection!")
  s[0..endIndex]

template rndElement*[T](s: openArray[T]): T =
  assert(s.len > 0, "Empty collection!")
  s[rand(s.high)]

proc contains*[T, V](sequence: openArray[T], s: openArray[V]): bool =
  result = false
  if(not sequence.isEmpty):
    for i in items(sequence):
      result = i in s

proc fold*[T, V, R](sequence: openArray[T], op: Reducer[V, R], init: R): R
  {.noSideEffect.} =
  result = init
  if(not sequence.isEmpty):
    for i in items(sequence):
      result = op(result, i)
